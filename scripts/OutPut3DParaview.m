[x,y,z] = meshgrid(0:pi/50:pi, 0:pi/50:pi, 0:pi/50:pi);
% Two Neumann wall on x axis.
vx = 2*cos(x).*cos(y).*cos(z);
vy = sin(x).*sin(y).*cos(z);
vz = sin(x).*cos(y).*sin(z);

B = cat(4,vx,vy,vz);
B = permute(B, [4 1 2 3]);
fid = fopen('TwoNeumanX.raw','w');
fwrite(fid,B,'float'); 
fclose(fid);